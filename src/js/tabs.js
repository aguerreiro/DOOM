function tabsHandler() {
    console.log('tabsHandler RUNNING');

    var component = $('.tabs'),
    componentNav = component.find('.tabs-nav'),
    componentNavItems = componentNav.find('li'),
    componentContents = component.find('.tabs-content');

    componentNavItems.on('click', function() {
        var targetID = $(this).data('id');

        console.log('Je clique sur ' + targetID);

        if ( $('#' + targetID).hasClass('tabs-content--inactive') ) {
            console.log('Mon élément est masqué');

            // Gestion de l'affichage du contenu
            componentContents.addClass('tabs-content--inactive');
            $('#' + targetID).removeClass('tabs-content--inactive');

            // Gestion de l'affichage du menu
            componentNavItems.removeClass('tabs-nav-item--active');
            $(this).addClass('tabs-nav-item--active');
        } else {
            console.log('Mon élément est visible');
        }
    });
}

$(document).ready(function () {
    console.log('JQuery et script chargé correctement');
    var tabsComponent = $('.tabs');
    if (tabsComponent.length > 0) {
        console.log('Mon composant tabs existe');
        tabsHandler();
    } else {
        console.log('Mon composant tabs n\'existe pas');
    }
});